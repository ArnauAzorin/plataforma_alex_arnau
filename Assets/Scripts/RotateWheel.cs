﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWheel : MonoBehaviour {
    private Rigidbody2D r2d;
    public float speed = 100.0f;

    void Start () {
        r2d = GetComponent<Rigidbody2D> ();
    }

    
    void FixedUpdate(){
        r2d.MoveRotation(r2d.rotation + speed * Time.fixedDeltaTime);
    }

    /// <summary>
    /// Sent when another object enters a trigger collider attached to this
    /// object (2D physics only).
    /// </summary>
    /// <param name="other">The other Collider2D involved in this collision.</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        speed*=-1;
    }
}