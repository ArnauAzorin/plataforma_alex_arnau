﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BoyController : MonoBehaviour{
    // Variables publicas para poder modifircar desde el inspector
    public float maxS = 4f;
    public float maxSShift = 11f;
    //Variables privadas
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    [SerializeField] private Animator anim;
    [SerializeField] private Transform graphics;
    [SerializeField] AudioSource audiojump;
    [SerializeField] AudioSource audiodead;
    
    private bool flipped = false;

    private bool jump = false;
    private bool grounded = false;
    private bool attack = false;

    private float speed = 5;
    private bool dead = false;



    // Use this for initialization
    void Awake () {
        // Obtenemos el rigidbody y lo guardamos en la variable rb2d
        // para poder utilizarla más cómodamente
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate () {

        if(dead){
            return;
        }
        
        //Miramos el input Horizontal
        move = Input.GetAxis("Horizontal");

        jump = Input.GetButtonDown("Jump");

        attack = Input.GetButtonDown("Fire1");

        if(jump && grounded){
            rb2d.velocity = new Vector2(move * speed, rb2d.velocity.y+speed);
        }else{
            rb2d.velocity = new Vector2(move * speed, rb2d.velocity.y);
        }

        if (jump)
        {
            audiojump.Play();
        }

        //Ponemos la velocidad horizontal y vertical
        anim.SetFloat("velocityH",Mathf.Abs(rb2d.velocity.x));
        anim.SetFloat("velocityV",Mathf.Abs(rb2d.velocity.y));
        anim.SetBool("grounded",grounded);

        if(rb2d.velocity.x<0){
            graphics.transform.localScale = new Vector3(-1,1,1);
            
        }else{
            graphics.transform.localScale = new Vector3(1,1,1);
            
        }

        /*
        rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
        //Miramos si nos estamos moviendo.
        // OJO!! Nunca comparar con 0 floats, nunca será 0 perfecto, siempre hay un error de redondeo
        if (rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f) {
            if ((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped)) {
                flipped = !flipped;
                this.transform.rotation = Quaternion.Euler(0,flipped?180:0,0);
            }
            anim.SetBool ("walking", true);
        } else {
            anim.SetBool ("walking", false);
        }
        */
    }

    private void OnCollisionEnter2D(Collision2D other) {
        grounded = true;
    }

    private void OnCollisionExit2D(Collision2D other) {
        grounded = false;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Killer"){
            audiodead.Play();
            StartCoroutine(death());
           
        }else if (other.tag == "win")
        {
            StartCoroutine(winner());
        }
    }
    IEnumerator winner()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("Win");
    }
    IEnumerator death()
    {
        dead = true;
        anim.SetTrigger("dead");
        
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("Lose");
    }
}
